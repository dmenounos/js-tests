Circle = Obj.extend(Obj, {

	x: 0,
	y: 0,
	radius: 1,
	color: "#000000",

	init: function() {
		this.setX(this.x);
		this.setY(this.y);
		this.setRadius(this.radius);
	},

	getX: function() {
		return this.x;
	},

	setX: function(x) {
		this.x = parseInt(x);
	},

	getY: function() {
		return this.y;
	},

	setY: function(y) {
		this.y = parseInt(y);
	},

	getRadius: function() {
		return this.radius;
	},

	setRadius: function(radius) {
		this.radius = parseFloat(radius);
	},

	getColor: function() {
		return this.color;
	},

	setColor: function(color) {
		this.color = color;
	},

	toString: function() {
		return 'Circle { '
		+ 'x: ' + this.x + ', '
		+ 'y: ' + this.y + ', '
		+ 'radius: ' + this.radius + ' }';
	},

	render: function(ctx) {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
		ctx.closePath();

		ctx.fillStyle = this.color;
		ctx.fill();
	}
});

Planet = Obj.extend(Circle, {

	degrees: 0,
	distance: 200,
	color: "#00FF00",

	update: function() {
		this.radians = toRadians(this.degrees++);
		this.setX(Math.cos(this.radians) * this.distance + Scene.halfWidth);
		this.setY(Math.sin(this.radians) * this.distance + Scene.halfHeight);
	}
});

Star = Obj.extend(Circle, {

	planet: new Circle(),
	planetMinVec: {},
	planetMaxVec: {},
	color: "#FF0000",

	update: function() {
		this.planet.setRadius(World.planet.radius);

		// 1) TRANSLATE PLANET TO OUR COORDINATE SYSTEM

		this.planet.setX(World.planet.x - this.x);
		this.planet.setY(World.planet.y - this.y);

		// 2) CALCULATE PLANET MAIN ANGLE RADIANS

		this.planetMainTan = this.planet.y / this.planet.x;
		this.planetMainRad = Math.atan(this.planetMainTan);
	//	console.log('this.planetMainTan: ' + this.planetMainTan);
	//	console.log('this.planetMainRad: ' + this.planetMainRad + ' ' + toDegrees(this.planetMainRad));

		if (this.planet.x < 0) {
			this.planetMainRad += Math.PI;
		}

		// 3) CALCULATE PLANET MAIN AND SIDE VECTOR LENGTHS

		this.planetMainLength = Math.sqrt(Math.pow(this.planet.x, 2) + Math.pow(this.planet.y, 2));
		this.planetSideLength = Math.sqrt(Math.pow(this.planet.radius, 2) + Math.pow(this.planetMainLength, 2));

		// 4) CALCULATE PLANET ANGLE BETWEEN MAIN AND SIDE VECTORS

		this.planetSideSin = World.planet.radius / this.planetMainLength;
		this.planetSideRad = Math.asin(this.planetSideSin);
	//	console.log('this.planetSideSin: ' + this.planetSideSin);
	//	console.log('this.planetSideRad: ' + this.planetSideRad + ' ' + toDegrees(this.planetSideRad));

		// 5.1) CALCULATE PLANET SIDE ANGLES (FULL)

		this.planetMinRad = this.planetMainRad - this.planetSideRad;
		this.planetMaxRad = this.planetMainRad + this.planetSideRad;

		// 5.2) CALCULATE PLANET SIDE VECTORS

		this.planetMinVec.y = Math.sin(this.planetMinRad) * this.planetSideLength;
		this.planetMinVec.x = Math.cos(this.planetMinRad) * this.planetSideLength;

		this.planetMaxVec.y = Math.sin(this.planetMaxRad) * this.planetSideLength;
		this.planetMaxVec.x = Math.cos(this.planetMaxRad) * this.planetSideLength;
	},

	render: function(ctx) {
		Star.superClass.prototype.render.apply(this, arguments);

		ctx.beginPath();
		ctx.moveTo(this.x, this.y);
		ctx.lineTo(this.x + this.planetMinVec.x, this.y + this.planetMinVec.y);
		ctx.moveTo(this.x, this.y);
		ctx.lineTo(this.x + this.planetMaxVec.x, this.y + this.planetMaxVec.y);
		ctx.closePath();

		ctx.strokeStyle = "#FF0000";
		ctx.stroke();
	}
});

World = {};

Scene = {};

function toDegrees(rads) {
	return rads * (180 / Math.PI);
}

function toRadians(degs) {
	return degs * (Math.PI / 180);
}

window.onload = function() {
	var canvas = document.getElementById('myCanvas');
	var ctx = canvas.getContext('2d');

	Scene.fullWidth  = parseInt(canvas.getAttribute('width'));
	Scene.fullHeight = parseInt(canvas.getAttribute('height'));

	Scene.halfWidth  = parseInt(Scene.fullWidth / 2);
	Scene.halfHeight = parseInt(Scene.fullHeight / 2);

	World.star = new Star({
	//	x: Math.random() * Scene.fullWidth,
	//	y: Math.random() * Scene.fullHeight,
		x: Scene.halfWidth,
		y: Scene.halfHeight,
		radius: 10
	});

	World.planet = new Planet({
		radius: 100
	});

	setInterval(function() {
		ctx.clearRect(0, 0, Scene.fullWidth, Scene.fullHeight);

		World.planet.update();
		World.planet.render(ctx);

		World.star.update();
		World.star.render(ctx);
	}, 25);
};
