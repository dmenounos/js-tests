Circle = Obj.extend(Obj, {

	x: 0,
	y: 0,
	radius: 1,

	init: function() {
		this.setX(this.x);
		this.setY(this.y);
		this.setRadius(this.radius);
	},

	getX: function() {
		return this.x;
	},

	setX: function(x) {
		this.x = parseInt(x);
	},

	getY: function() {
		return this.y;
	},

	setY: function(y) {
		this.y = parseInt(y);
	},

	getRadius: function() {
		return this.radius;
	},

	setRadius: function(radius) {
		this.radius = parseFloat(radius);
	},

	render: function(ctx) {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
		ctx.closePath();

		ctx.fillStyle = "#000000";
		ctx.fill();
	}
});

Radar = Obj.extend(Circle, {

	degrees: 0,
	beam: {},

	update: function() {
		this.radians = toRadians(this.degrees++);
		this.beam.x = Math.cos(this.radians) * this.getRadius();
		this.beam.y = Math.sin(this.radians) * this.getRadius();
	},

	render: function(ctx) {
		Radar.superClass.prototype.render.apply(this, arguments);

		ctx.beginPath();
		ctx.moveTo(this.x, this.y);
		ctx.lineTo(this.x + this.beam.x, this.y + this.beam.y);
		ctx.closePath();

		ctx.lineWidth = 5;
		ctx.strokeStyle = "#FFFFFF";
		ctx.stroke();
	}
});

function toDegrees(rads) {
	return rads * (180 / Math.PI);
}

function toRadians(degs) {
	return degs / (180 / Math.PI);
}

$(function() {
	var canvas = $('#myCanvas');

	var ctx = canvas.get(0).getContext('2d');

	var fullWidth  = parseInt(canvas.attr('width'));
	var fullHeight = parseInt(canvas.attr('height'));

	var halfWidth  = parseInt(fullWidth / 2);
	var halfHeight = parseInt(fullHeight / 2);

	var radar = new Radar({
		x: halfWidth,
		y: halfHeight,
		radius: Math.min(halfWidth, halfHeight)
	});

	// 40 FRAMES PER SECOND
	setInterval(function() {
		ctx.clearRect(0, 0, fullWidth, fullHeight);

		radar.update();
		radar.render(ctx);

	}, 25);

	$('#myButton').on('click', function() {
		var now = Date.now();
		var end = now + 3000; // 3 seconds

		while ((now = Date.now()) < end) {
			// keep thread busy
		}
	});
});
