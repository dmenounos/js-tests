(function($) {

	var config = {
		prevSelector: '.prev',
		nextSelector: '.next',
		disabledClass: 'disabled',
		initialIndex: 0,
		easing: 'swing',
		speed: 400
	};

	function Slider(root, options) {
		this.root = root;
		this.options = options;

		this.initScene();

		// init navigation link
		this.prev = $(options.prevSelector);
		this.prev.on('click', $.proxy(function(e) {
			e.preventDefault();
			this.movePrev();
		}, this));

		// init navigation link
		this.next = $(options.nextSelector)
		this.next.on('click', $.proxy(function(e) {
			e.preventDefault();
			this.moveNext();
		}, this));

		// init custom event handlers
		$.each(['onBeforeSeek', 'onSeek'], $.proxy(function(i, name) {
			if ($.isFunction(this.options[name])) { 
				$(this).on(name, this.options[name]);
			}
		}, this));

		// init navigation event handler
		$(this).on('onSeek', function(e, idx) {
			this.prev.toggleClass(this.options.disabledClass, idx == 0);
			this.next.toggleClass(this.options.disabledClass, idx == this.getSize() - 1);
		});

		this.index = 0;
		this.seekTo(options.initialIndex, 0);
	}

	$.extend(Slider.prototype, {

		initScene: function() {
			this.root.css('position', 'relative');
			this.root.css('overflow', 'hidden');

			var offset = 0;
			var itemWidth = 0;
			var itemHeight = 0;

			$.each(this.getItems(), $.proxy(function(index, item) {
				var $item = $(item);

				itemWidth  = $item.outerWidth(true);
				itemHeight = $item.outerHeight(true);

				var $itemWrapper = $("<div></div>");
				$itemWrapper.css('position', 'absolute');
				$itemWrapper.css('width',  itemWidth);
				$itemWrapper.css('height', itemHeight);
				$itemWrapper.css('left', offset);

				$item.appendTo($itemWrapper);
				$itemWrapper.appendTo(this.root);

				offset += itemWidth;
			}, this));

			if (this.options.width == 'auto') {
				this.root.css('width', itemWidth);
			}
			else if (this.options.width) {
				this.root.css('width', this.options.width);
			}

			if (this.options.height == 'auto') {
				this.root.css('height', itemHeight);
			}
			else if (this.options.height) {
				this.root.css('height', this.options.height);
			}
		},

		getItems: function() {
			return this.root.children();
		},

		getItem: function(index) {
			return this.getItems().eq(index);
		},

		getSize: function() {
			return this.getItems().size();
		},

		seekTo: function(idx, time) {
			if (idx < 0 || idx >= this.getSize()) {
				return; // out of bounds
			}

			if (this.busy) {
				return;
			}

			var e = $.Event("onBeforeSeek");
			$(this).trigger(e, [ idx ]);
			if (e.isDefaultPrevented()) {
				return;
			}

			this.busy = true;

			// sum distance
			var $item, width = 0;
			var min = Math.min(this.index, idx);
			var max = Math.max(this.index, idx);

			for (var x = min; x < max; x++) {
				$item = this.getItem(x).children(":first");
				width += $item.outerWidth(true);
			}

			if (this.index < idx) {
				width = -width;
			}

			// get current offset
			$item = this.getItem(this.index);
			var left = parseInt($item.css('marginLeft'));

			// animation properties
			var props = { marginLeft: width + left + 'px' };

			this.getItems().animate(props, {
				duration: time || this.options.speed,
				easing: this.options.easing,
				complete: $.proxy(function() {
					var e = $.Event("onSeek");
					$(this).trigger(e, [ idx ]);
					this.index = idx;
					delete this.busy;
				}, this)
			});
		},

		movePrev: function(time) {
			this.seekTo(this.index - 1, time);
		},

		moveNext: function(time) {
			this.seekTo(this.index + 1, time);
		}
	});

	$.fn.slider = function(options) {
		options = $.extend({}, config, options);

		return this.each(function() {
			new Slider($(this), options);
		});
	};

})(jQuery);

